from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from resources.base_resource import UserAPI


app = Flask(__name__)
api = Api(app)
db = SQLAlchemy(app)
app.config.from_object('config.Config')



api.add_resource(UserAPI, '/users/', endpoint='user')

if __name__ == '__main__':
    app.run(debug=True)
